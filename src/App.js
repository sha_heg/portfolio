import React from "react";
import "./App.css";
import { Layout, Header, Navigation, Content, Drawer } from "react-mdl";
import Main from "./Components/main";
import { Link } from "react-router-dom";

function App() {
  return (
    <div className="demo-big-content">
      <Layout>
        <Header
          className="header"
          title='Resume'
        >
          <Navigation>
            <Link to="/Contact">Contact</Link>
            <Link to="/">Home</Link>
          </Navigation>
         
        </Header>
         <Drawer >
            <Navigation>
            <Link to="/Contact">Contact</Link>
            <Link to="/">Home</Link>
            </Navigation>
        </Drawer>
        <Content>
          <div className="page-content" />
          <Main />
        </Content>
      </Layout>
    </div>
  );
}

export default App;
