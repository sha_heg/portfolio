import React from "react";
import { Grid, Cell } from "react-mdl";
import photo from "./photo.png";
function Landing() {
  return (
    <div style={{ width: "100%", margin: "auto" }}>
      <Grid className="landing-gird">
        <Cell col={12}>
          <img src={photo} alt="avatar" className="avatar" />
          
            <h1> Shamanth Hegde</h1>
            <hr />
              <h4> Data Analysis:</h4>
            <ul>
              <li><strong>Excel:</strong> Table, pivot table, pivot charts, data tools, macros, 
              data validation, conditional formatting, structured 
reference, solver, goal seek, Functions, conditions</li>
              <li><strong>Access:</strong> Forms, Queries, Forms, Report</li>
              <li> <strong>SQL:</strong> Group by, joins, nested queries, functions, Triggers.</li>
              <li><strong>R:</strong>  tidyr| dplyr| ggplot2 | data.table</li>
              <li><strong>Python:</strong>  pandas | numpy | seaborn | matplotlib | plotly | 
Folium</li>
            </ul>

            <h4>Web Development:</h4>
            <ul>
              <li>HTML | CSS | Bootstrap | Javascript | React.js | reactbootstrap</li>
            </ul>

            <h4>Version control:</h4>
            <ul>
              <li>Git | GitHub | Bitbucket | Sourcetree</li>
            </ul>
         
        </Cell>
      </Grid>
    </div>
  );
}

export default Landing;
