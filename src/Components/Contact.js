import React from "react";
import { Grid, Cell, List, ListItem, ListItemContent } from "react-mdl";
import photo from "./photo.png";

function Contact() {
  return (
 
    <div className="contact"  style={{ width: "100%", margin: "auto" }}>

      <Grid className="contact-grid">
        <Cell col={6}>
          <h2>Shamanth</h2>
          <img src={photo} alt="avatar" style={{ height: "300px" }}></img>
          <p style={{ width: "75%", margin: "auto", paddingTop: "1em",color: 'black' }}>
            I am Shamanth Hegde. B.Tech in Print and MediaTechology with the
            minor specialization in Data Science. Pursued B.Tech at MIT Manipal
            in 2019. Very interested in Datas , Web devolopment, and Mobile app
            Development. 
          </p>
        </Cell>
        <Cell col={6}>
          <h2>Contact</h2>

          <hr></hr>

          <div className="contact-list">
            <List>
              <ListItem>
                <ListItemContent
                  style={{ fontsize: "50px", fontFamily: "Anton" }}
                >
                  <i className="fa fa-phone-square" aria-hidden="true" />
                  (+91) 7019244485
                </ListItemContent>
              </ListItem>
              <ListItem>
                <ListItemContent
                  style={{ fontsize: "50px", fontFamily: "Anton" }}
                >
                  <i className="fa fa-envelope-square" aria-hidden="true" />
                  shamanth7@hotmail.com
                </ListItemContent>
              </ListItem>

              <ListItem>
                <ListItemContent
                  style={{ fontsize: "50px", fontFamily: "Anton" }}
                >
                  <i class="fa fa-university" aria-hidden="true"></i>
                  MIT Manipal
                </ListItemContent>
              </ListItem>
            </List>
          </div>
        </Cell>
      </Grid>
      </div>
     
  );
}

export default Contact;
