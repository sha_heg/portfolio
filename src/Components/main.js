import React from "react";
import Landing from "./Landing";
import Contact from "./Contact";
import { Switch, Route } from "react-router-dom";
const Main = () => (
  <Switch>
    <Route exact path="/" component={Landing} />
    <Route path="/Contact" component={Contact} />
    <Route path="/Home" component={Landing} />
  </Switch>
);

export default Main;
